scenario = "abnon400";
default_font_size = 32;
default_font = "Arial";
active_buttons = 5;
button_codes=201,202,203,204,205;
response_matching = simple_matching;
write_codes = true;

begin;


array {
	LOOP $i 8; $nr = '$i+1';								
		trial{
			trial_type = first_response;
			trial_duration = forever;
			terminator_button = 1,2;
			picture {bitmap {filename = "adnon400_instruktionen/Folie$nr.jpg"; }; x = 0; y = 0;};
			duration = response;
		};		
	ENDLOOP;
} instructions;


picture { bitmap {filename="likert5.png"; }; x = 0; y = 0;} likert;

trial{
	trial_type = fixed;
	all_responses = false;
	stimulus_event{ #Fixation				
		picture {text {caption="Bitte schneller antworten";  }; x = 0; y = 0;};
		response_active = false;
		duration = '1000';
	};
} schneller; 

array {
	TEMPLATE "uebstimulus.tem" {
		prime target condition;
		TEMPLATE "uebung.tem";
	};
}uebung;

array {
	TEMPLATE "stimulus.tem" {
		prime target condition ptrig ttrig;
		TEMPLATE "block1.tem";
	};
}stimuli1;

array {
	TEMPLATE "stimulus.tem" {
		prime target condition ptrig ttrig;
		TEMPLATE "block2.tem";
	};
}stimuli2;

array {
	TEMPLATE "stimulus.tem" {
		prime target condition ptrig ttrig;
		TEMPLATE "block3.tem";
	};
}stimuli3;

array {
	LOOP $i 150;
		trial{
			trial_type = fixed;
			all_responses = false;
			stimulus_event{ #iti				
				picture {text {caption=" ";}; x = 0; y = 0;};
				response_active = false;
				duration = '1000 + int($random_value * 1000)';
			};
		} ; 
	ENDLOOP;
} iti;

trial{
	trial_type = first_response;
	trial_duration = forever;
	terminator_button = 1,2;
	picture {text {caption="Kurze Pause, weiter mit Tastendruck";  }; x = 0; y = 0;};
	duration = response;
} pause;	

begin_pcl;   ############################################################# PCL ############################################################################


instructions[1].present();
instructions[2].present();
instructions[3].present();

iti.shuffle();
uebung.shuffle();
loop int i=1 until i > uebung.count() begin
	uebung[i].present();
	iti[i].present();
	i=i+1;
end;

instructions[4].present();

iti.shuffle();
stimuli1.shuffle();
loop int i=1 until i > stimuli1.count() begin
	stimuli1[i].present();
	iti[i].present();
	if mod(i,20)==0 then
		pause.present();
	end;
	i=i+1;
end;

instructions[5].present();


stimuli2.shuffle();
loop int i=1 until i > stimuli2.count() begin
	stimuli2[i].present();
	iti[50 + i].present();
	if mod(50+i,20)==0 then
		pause.present();
	end;
	i=i+1;
end;

instructions[6].present();

stimuli3.shuffle();
loop int i=1 until i > stimuli3.count() begin
	stimuli3[i].present();
	iti[100 + i].present();
	if mod(100+i,20)==0 then
		pause.present();
	end;
	i=i+1;
end;

instructions[7].present();