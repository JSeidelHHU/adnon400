$fix_time= 0;
$prime_time= '1000 + int($random_value * 1000)';
$target_time= '$prime_time + 200';
$blank_time= '$target_time + 1000';
$likert_time= '$blank_time +  500 + int($random_value * 500)';

trial{
	trial_type = correct_response;
	no_response_feedback = schneller;
	all_responses = false;
	stimulus_event{ #fix 		
		picture {text {caption="+";  font="Times New Roman";  font_size=30; }; x = 0; y = 0;};
		response_active = false;
		port_code = 99;
	}; 
	stimulus_event{ #prime 		
		picture {text {caption="$prime";   }; x = 0; y = 0;};
		response_active = false;
		time = $prime_time;
		#port_code = $ptrig;
	}; 	
	stimulus_event{ #target 		
		picture {text {caption="$target";   }; x = 0; y = 0;};
		response_active = false;
		time = $target_time;
		#port_code = $ttrig;
	}; 	
	stimulus_event{ #blank 		
		picture {text {caption=" ";   }; x = 0; y = 0;};
		response_active = false;
		time = $blank_time;
	}; 	
	stimulus_event{ #likert 		
		picture likert;
		response_active = true;
		time = $likert_time;
		target_button= 1,2,3,4,5;
		duration = '$likert_time + 4000';
	}; 
}; 